package at.campus02.nowa.prg3.quiz.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import at.campus02.nowa.prg3.quiz.protocol.ClientAnswer;
import at.campus02.nowa.prg3.quiz.protocol.HighscoreMessage;
import at.campus02.nowa.prg3.quiz.protocol.PlayerLogin;
import at.campus02.nowa.prg3.quiz.protocol.QuestionMessage;
import at.campus02.nowa.prg3.quiz.protocol.ServerAnswer;

public class Client {

	public static void main(String[] args) {
		try (
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			Socket client = new Socket(InetAddress.getLocalHost(), 1234);
			ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
		) {
			oos.flush();
			System.out.println("Spielername:");
			String name = br.readLine();
			oos.writeObject(new PlayerLogin(name));
			oos.flush();
			while (true) {
				QuestionMessage qm = (QuestionMessage) ois.readObject();
				System.out.println("Frage: " + qm.getText());
				Map<String, UUID> selection = new HashMap<>();
				int index = 1;
				for (Entry<UUID, String> answer : qm.getAnswers().entrySet()) {
					System.out.println(" [" + index + "]: " + answer.getValue());
					selection.put(Integer.toString(index), answer.getKey());
					index++;
				}
				UUID id = null;
				while (id == null) {
					System.out.println("Ihre Antwort:");
					String input = br.readLine();
					id = selection.get(input);
				}
				oos.writeObject(new ClientAnswer(id));
				oos.flush();
				ServerAnswer sa = (ServerAnswer) ois.readObject();
				if (sa.getCorrect()) {
					System.out.println("Ihre Antwort war korrekt!");
				} else {
					System.out.println("Ihre Antwort war leider FALSCH!");
				}
				System.out.format("Sie haben derzeit %d Punkte!\n", sa.getPoints());
				if (sa.getEnd()) {
					break;
				}
			}
			HighscoreMessage hm = (HighscoreMessage) ois.readObject();
			System.out.println("Highscore:");
			for (Entry<String, Integer> score : hm.getHighscore().entrySet()) {
				System.out.println(" " + score.getKey() + ": " + score.getValue());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
