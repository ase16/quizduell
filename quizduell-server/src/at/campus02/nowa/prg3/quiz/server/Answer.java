package at.campus02.nowa.prg3.quiz.server;

import java.util.UUID;

public class Answer {

	private UUID id;
	private String text;
	private boolean correct;

	public Answer(String text, boolean correct) {
		id = UUID.randomUUID();
		this.text = text;
		this.correct = correct;
	}

	public UUID getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public boolean isCorrect() {
		return correct;
	}

}
