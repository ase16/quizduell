package at.campus02.nowa.prg3.quiz.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CsvQuestionRepository implements IQuestionRepository, Cloneable {

	private File file;
	private List<Question> questions = new ArrayList<>();

	public CsvQuestionRepository(File file) {
		super();
		this.file = file;
		readFile();
	}

	private void readFile() {
		try (BufferedReader br = new BufferedReader(new FileReader(file));) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] fields = line.replace("\"", "").split(";", -1);
				try {
					Integer.valueOf(fields[6]);
				} catch (NumberFormatException e) {
					continue;
				}
				Question q = new Question(fields[0]);
				for (int i = 1; i < 5; i++) {
					/*
					 * if (i == 1) { Answer a = new Answer(fields[i], true); }
					 * else { Answer a = new Answer(fields[i], false); }
					 */

					Answer a = new Answer(fields[i], i == 1);
					q.addAnswer(a);
				}
				questions.add(q);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Question> getList() {

		return questions;
	}

	@Override
	public void reload() {
		questions.clear();
		readFile();
	}

}
