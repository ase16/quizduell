package at.campus02.nowa.prg3.quiz.server;

import java.util.List;

public interface IQuestionRepository {
	
	public List<Question> getList();
	
	public void reload();

}
