package at.campus02.nowa.prg3.quiz.server;

import java.util.ArrayList;
import java.util.List;

public class Question {
	
	private String text;
	private List<Answer> answers = new ArrayList<>();
	
	public Question(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public List<Answer> getAnswers() {
		return answers;
	}
	
	public void addAnswer(Answer a) {
		answers.add(a);
	}
	
	

}
