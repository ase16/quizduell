package at.campus02.nowa.prg3.quiz.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import at.campus02.nowa.prg3.quiz.protocol.ClientAnswer;
import at.campus02.nowa.prg3.quiz.protocol.HighscoreMessage;
import at.campus02.nowa.prg3.quiz.protocol.PlayerLogin;
import at.campus02.nowa.prg3.quiz.protocol.QuestionMessage;
import at.campus02.nowa.prg3.quiz.protocol.ServerAnswer;

public class GameWorker implements Runnable {

	private static final Integer maxQuestions = 10;

	private Socket connection;
	private IQuestionRepository repo;
	private Map<String, Integer> highscore;

	public GameWorker(Socket connection, IQuestionRepository repo, Map<String, Integer> highscore) {
		super();
		this.connection = connection;
		this.repo = repo;
		this.highscore = highscore;
	}

	@Override
	public void run() {
		try (ObjectInputStream ois = new ObjectInputStream(connection.getInputStream());
				ObjectOutputStream oos = new ObjectOutputStream(connection.getOutputStream());) {
			oos.flush();
			List<Question> questions = repo.getList();
			Set<Question> questionSet = new HashSet<>();
			Random rand = new Random();
			if (questions.size() < maxQuestions) {
				return;
			}
			while (questionSet.size() < maxQuestions) {
				Question q = questions.get(rand.nextInt(questions.size()));
				questionSet.add(q);
			}
			PlayerLogin pl = (PlayerLogin) ois.readObject();
			int points = 0;
			int remaining = questionSet.size();
			System.out.println("Spieler angemeldet: " + pl.getName());
			for (Question q : questionSet) {
				remaining--;
				Map<UUID, Boolean> answers = new HashMap<>();
				QuestionMessage qm = new QuestionMessage(q.getText());
				for (Answer a : q.getAnswers()) {
					qm.addAnswer(a.getId(), a.getText());
					answers.put(a.getId(), a.isCorrect());
				}
				oos.writeObject(qm);
				oos.flush();
				ClientAnswer ca = (ClientAnswer) ois.readObject();
				Boolean correct = answers.get(ca.getAnswer());
				if (correct) {
					points++;
				}
				oos.writeObject(new ServerAnswer(correct, remaining == 0, points));
				oos.flush();
			}
			synchronized (highscore) {
				Integer oldScore = highscore.getOrDefault(pl.getName(), 0);
				if (points > oldScore) {
					highscore.put(pl.getName(), points);
				}
			}
			oos.writeObject(new HighscoreMessage(highscore));
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			connection.close();
		} catch (IOException e) {
			System.out.println("Connection not closed properly.");
		}
	}

}
