package at.campus02.nowa.prg3.quiz.server;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Server {

	public static void main(String[] args) {
		File csv = new File("quizfragen.csv");
		IQuestionRepository qr = new CsvQuestionRepository(csv);
		Map<String, Integer> highscore = new HashMap<>();
		try (ServerSocket server = new ServerSocket(1234);) {
			while (true) {
				Socket connection = server.accept();	
				GameWorker gw = new GameWorker(connection, qr, highscore);
				new Thread(gw).start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
