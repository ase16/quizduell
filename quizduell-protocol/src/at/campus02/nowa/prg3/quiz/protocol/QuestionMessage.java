package at.campus02.nowa.prg3.quiz.protocol;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class QuestionMessage implements Serializable {

	private static final long serialVersionUID = -3760377756758680560L;
	
	private String text;
	private Map<UUID, String> answers = new HashMap<>();

	public QuestionMessage(String text) {
		super();
		this.text = text;
	}
	
	public void addAnswer(UUID id, String text) {
		answers.put(id, text);
	}
	
	public String getText() {
		return text;
	}

	public Map<UUID, String> getAnswers() {
		return answers;
	}

	
	

}
