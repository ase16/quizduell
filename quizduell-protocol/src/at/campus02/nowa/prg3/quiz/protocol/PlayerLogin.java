package at.campus02.nowa.prg3.quiz.protocol;

import java.io.Serializable;

public class PlayerLogin implements Serializable {

	private static final long serialVersionUID = 7724736665408451620L;

	private String name;

	public PlayerLogin(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
