package at.campus02.nowa.prg3.quiz.protocol;

import java.io.Serializable;
import java.util.UUID;

public class ClientAnswer implements Serializable {

	private static final long serialVersionUID = -5412858241165052313L;

	private UUID answer;

	public ClientAnswer(UUID answer) {
		super();
		this.answer = answer;
	}

	public UUID getAnswer() {
		return answer;
	}

}
