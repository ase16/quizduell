package at.campus02.nowa.prg3.quiz.protocol;

import java.io.Serializable;
import java.util.Map;

public class HighscoreMessage implements Serializable {

	private static final long serialVersionUID = 4957481132331122765L;

	private Map<String, Integer> highscore;

	public HighscoreMessage(Map<String, Integer> highscore) {
		super();
		this.highscore = highscore;
	}

	public Map<String, Integer> getHighscore() {
		return highscore;
	}

}
