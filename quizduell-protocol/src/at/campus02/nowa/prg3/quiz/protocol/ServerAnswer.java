package at.campus02.nowa.prg3.quiz.protocol;

import java.io.Serializable;

public class ServerAnswer implements Serializable {

	private static final long serialVersionUID = -2652612527040735046L;

	private Boolean correct;
	private Boolean end;
	private Integer points;

	public ServerAnswer(Boolean correct, Boolean end, Integer points) {
		super();
		this.correct = correct;
		this.end = end;
		this.points = points;
	}

	public Integer getPoints() {
		return points;
	}

	public Boolean getEnd() {
		return end;
	}

	public Boolean getCorrect() {
		return correct;
	}

}
